package com.noroof.assignment1;

    public class InvalidArmorException extends Exception{
    // Custom constructor
    public InvalidArmorException(){
        super("Exception: This armor can't be used by this hero or you are below the required level");
    }
}
