package com.noroof.assignment1;

public class Ranger extends Hero{
    // Custom constructor
    public Ranger(String name){
        super(name, 1, new Hero_Attributes(1, 7, 1),
                new Weapon.WeaponType[]{Weapon.WeaponType.Bow},
                new Armor.ArmorType[]{Armor.ArmorType.Leather, Armor.ArmorType.Mail});
    }
    // Methods
    // Increase level and levelAttributes fields
    public void levelUp(){
        this.level += 1;
        this.levelAttributes = Hero_Attributes.add(this.levelAttributes, new Hero_Attributes(1, 5, 1));
    }
    // Calculate Ranger's damage
    public float damage(){
        if (this.equipment.get(Slot.Weapon) == null)
            return 1 + totalAttributes().getDexterity() / 100.0f;
        return this.equipment.get(Slot.Weapon).weaponDamage * (1 + totalAttributes().getDexterity() / 100.0f);
    }
    // Display Ranger's stats
    public String display(){
        return  "----------------------------------------" + "\n" +
                "Name: " + this.name + "\n" +
                "Class: " + "Ranger" + "\n" +
                "Level: " + this.level + "\n" +
                "Total strength: " + totalAttributes().getStrength() + "\n" +
                "Total dexterity: " + totalAttributes().getDexterity() + "\n" +
                "Total intelligence: " + totalAttributes().getIntelligence() + "\n" +
                "Damage: " + damage() + "\n" +
                "----------------------------------------";
    }
}
