package com.noroof.assignment1;

public class Hero_Attributes {
    // Variables
    private int strength;
    private int dexterity;
    private int intelligence;
    // Custom constructor
    public Hero_Attributes(int strength, int dexterity, int intelligence){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
    // Methods
    // Return hero's strength
    public int getStrength() {
        return this.strength;
    }
    // Return hero's dexterity
    public int getDexterity() {
        return this.dexterity;
    }
    // Return hero's intelligence
    public int getIntelligence() {
        return this.intelligence;
    }
    // Return hero's attributes after level up or to calculate totalAttributes
    public static Hero_Attributes add(Hero_Attributes h1, Hero_Attributes h2){
        return new Hero_Attributes(h1.getStrength() + h2.getStrength(), h1.getDexterity() +h2.getDexterity(), h1.getIntelligence() + h2.getIntelligence());
    }
}
