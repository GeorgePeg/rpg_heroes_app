package com.noroof.assignment1;

public class Mage extends Hero{
    // Custom constructor
    public Mage(String name){
        super(name, 1, new Hero_Attributes(1, 1, 8),
                new Weapon.WeaponType[]{Weapon.WeaponType.Staff, Weapon.WeaponType.Wand},
                new Armor.ArmorType[]{Armor.ArmorType.Cloth});
    }
    // Methods
    // Increase level and levelAttributes fields
    public void levelUp(){
        this.level += 1;
        this.levelAttributes = Hero_Attributes.add(this.levelAttributes, new Hero_Attributes(1, 1, 5));
    }
    // Calculate Mage's damage
    public float damage(){
        if (this.equipment.get(Slot.Weapon) == null)
            return 1 + totalAttributes().getIntelligence() / 100.0f;
        return this.equipment.get(Slot.Weapon).weaponDamage * (1 + totalAttributes().getIntelligence() / 100.0f);
    }
    // Display Mage's stats
    public String display(){
        return  "----------------------------------------" + "\n" +
                "Name: " + this.name + "\n" +
                "Class: " + "Mage" + "\n" +
                "Level: " + this.level + "\n" +
                "Total strength: " + totalAttributes().getStrength() + "\n" +
                "Total dexterity: " + totalAttributes().getDexterity() + "\n" +
                "Total intelligence: " + totalAttributes().getIntelligence() + "\n" +
                "Damage: " + damage() + "\n" +
                "----------------------------------------";
    }
}
