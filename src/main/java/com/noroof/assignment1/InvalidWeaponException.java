package com.noroof.assignment1;

public class InvalidWeaponException extends Exception{
    // Custom constructor
    public InvalidWeaponException(){
        super("Exception: This weapon can't be used by this hero or you are below the required level");
    }
}
