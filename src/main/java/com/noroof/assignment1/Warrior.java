package com.noroof.assignment1;

public class Warrior extends Hero{
    // Custom constructor
    public Warrior(String name){
        super(name, 1, new Hero_Attributes(5, 2, 1),
                new Weapon.WeaponType[]{Weapon.WeaponType.Axe, Weapon.WeaponType.Hammer, Weapon.WeaponType.Sword},
                new Armor.ArmorType[]{Armor.ArmorType.Mail, Armor.ArmorType.Plate});
    }
    // Methods
    // Increase level and levelAttributes fields
    public void levelUp(){
        this.level += 1;
        this.levelAttributes = Hero_Attributes.add(this.levelAttributes, new Hero_Attributes(3, 2, 1));
    }
    // Display Warrior's stats
    public String display(){
        return  "----------------------------------------" + "\n" +
                "Name: " + this.name + "\n" +
                "Class: " + "Warrior" + "\n" +
                "Level: " + this.level + "\n" +
                "Total strength: " + totalAttributes().getStrength() + "\n" +
                "Total dexterity: " + totalAttributes().getDexterity() + "\n" +
                "Total intelligence: " + totalAttributes().getIntelligence() + "\n" +
                "Damage: " + damage() + "\n" +
                "----------------------------------------";
    }
}
