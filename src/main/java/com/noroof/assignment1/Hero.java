package com.noroof.assignment1;

import java.util.HashMap;

public abstract class Hero {
    // Variables
    protected String name;
    protected int level;
    protected Hero_Attributes levelAttributes;
    protected HashMap<Slot, Item> equipment;
    protected Weapon.WeaponType[] validWeaponTypes;
    protected Armor.ArmorType[] validArmorTypes;
    // Custom constructor
    public Hero(String name, int level, Hero_Attributes levelAttributes, Weapon.WeaponType[] validWeaponTypes, Armor.ArmorType[] validArmorTypes){
        this.name = name;
        this.level = level;
        this.levelAttributes = levelAttributes;
        this.equipment = new HashMap<>();
        for(Slot s : Slot.values())
            this.equipment.put(s, null);
        this.validWeaponTypes = validWeaponTypes;
        this.validArmorTypes = validArmorTypes;
    }
    // Methods
    // Increase level and levelAttributes fields
    public void levelUp(){
        this.level += 1;
        this.levelAttributes = Hero_Attributes.add(this.levelAttributes, new Hero_Attributes(1, 1, 1));
    }
    // Equip an armor
    public void equip(Armor armor) throws InvalidArmorException {
        boolean flag = true;
        for(Armor.ArmorType a : this.validArmorTypes)
            if(armor.armorType.equals(a) && armor.requiredLevel <= this.level) {
                this.equipment.replace(armor.slot, armor);
                flag = false;
                break;
            }
        if(flag)
            throw new InvalidArmorException();
    }
    // Equip a weapon
    public void equip(Weapon weapon) throws InvalidWeaponException {
        boolean flag = true;
        for(Weapon.WeaponType w : validWeaponTypes)
            if(weapon.weaponType.equals(w) && weapon.requiredLevel <= this.level){
                equipment.replace(weapon.slot, weapon);
                flag = false;
                break;
            }
        if(flag)
            throw new InvalidWeaponException();
    }
    // Calculate Hero's damage
    public float damage(){
        if(this.equipment.get(Slot.Weapon) == null)
            return 1 + totalAttributes().getStrength() / 100.0f;
        return this.equipment.get(Slot.Weapon).weaponDamage * (1 + totalAttributes().getStrength() / 100.0f);
    }
    // Calculates Hero's total attributes
    public Hero_Attributes totalAttributes(){
        Hero_Attributes total = new Hero_Attributes(0, 0, 0);
        for(Slot s : new Slot[]{Slot.Head, Slot.Body, Slot.Legs})
            if(this.equipment.get(s) != null)
                total = Hero_Attributes.add(total, this.equipment.get(s).ArmorAttribute);
        return Hero_Attributes.add(total, levelAttributes);
    }
    // Display Hero's stats
    public String display(){
        return  "----------------------------------------" + "\n" +
                "Name: " + this.name + "\n" +
                "Class: " + "Hero" + "\n" +
                "Level: " + this.level + "\n" +
                "Total strength: " + totalAttributes().getStrength() + "\n" +
                "Total dexterity: " + totalAttributes().getDexterity() + "\n" +
                "Total intelligence: " + totalAttributes().getIntelligence() + "\n" +
                "Damage: " + damage() + "\n" +
                "----------------------------------------";
    }
}
