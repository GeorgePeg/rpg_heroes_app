package com.noroof.assignment1;

public abstract class Item {
    // Variables
    protected String name;
    protected int requiredLevel;
    protected Slot slot;
    protected int weaponDamage;
    Weapon.WeaponType weaponType;
    protected Hero_Attributes ArmorAttribute;
    Armor.ArmorType armorType;
    // Custom constructor
    public Item(String name, int requiredLevel, Slot slot, int weaponDamage, Weapon.WeaponType weaponType,
                Hero_Attributes ArmorAttribute, Armor.ArmorType armorType){
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.weaponDamage = weaponDamage;
        this.weaponType = weaponType;
        this.ArmorAttribute = ArmorAttribute;
        this.armorType = armorType;
    }
}
