package com.noroof.assignment1;

public class Weapon extends Item{
    protected enum WeaponType{Axe, Bow, Dagger, Hammer, Staff, Sword, Wand}
    // Custom constructor
    public Weapon(String name, int requiredLevel, int weaponDamage, Weapon.WeaponType weaponType) {
        super(name, requiredLevel, Slot.Weapon, weaponDamage, weaponType, null, null);
    }
}
