package com.noroof.assignment1;

public class Armor extends Item{
    protected enum ArmorType {Cloth, Leather, Mail, Plate}
    // Custom constructor
    public Armor(String name, int requiredLevel, Slot slot, Hero_Attributes ArmorAttribute,
                 Armor.ArmorType armorType) {
        super(name, requiredLevel, slot, 0, null, ArmorAttribute, armorType);
    }
}
