package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    @Test
    void checkArmorAttributesTest(){
        Armor a = new Armor("Magic Cloth", 1, Slot.Body, new Hero_Attributes(1,1,1),
                Armor.ArmorType.Cloth);                                 // Create armor
        assertEquals("Magic Cloth", a.name);                   // Check name
        assertEquals(1, a.requiredLevel);                      // Check required level
        assertEquals(Slot.Body, a.slot);                               // Check slot
        assertEquals(Armor.ArmorType.Cloth, a.armorType);              // Check armor type
        assertEquals(1, a.ArmorAttribute.getStrength());       // Check armor's strength
        assertEquals(1, a.ArmorAttribute.getDexterity());      // Check armor's dexterity
        assertEquals(1, a.ArmorAttribute.getIntelligence());   // Check armor's intelligence
    }
}