package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void constructorTest(){
        Rogue r = new Rogue("Valeera");                         // Create a Rogue object
        assertEquals("Valeera", r.name);                      // Check name
        assertEquals(1, r.level);                             // Check level
        assertEquals(2, r.levelAttributes.getStrength());     // Check strength
        assertEquals(6, r.levelAttributes.getDexterity());    // Check dexterity
        assertEquals(1, r.levelAttributes.getIntelligence()); // intelligence
    }

    @Test
    void levelUpTest() {
        Rogue r = new Rogue("Valeera");                           // Create a Rogue object
        r.levelUp();                                                    // Level up
        assertEquals(2, r.level);                               // Check level
        assertEquals(3, r.levelAttributes.getStrength());       // Check strength
        assertEquals(10, r.levelAttributes.getDexterity());     // Check dexterity
        assertEquals(2, r.levelAttributes.getIntelligence());   // intelligence
    }

    @Test
    void equipACorrectWeaponTest() throws InvalidWeaponException {
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        r.equip(new Weapon("Poison Dagger", 1, 2, Weapon.WeaponType.Dagger)); // Equip weapon
    }

    @Test
    void equipAWrongWeaponTest(){
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        try {
            r.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectWeaponNotRequiredLevelTest(){
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        try {
            r.equip(new Weapon("Poison Dagger", 2, 2, Weapon.WeaponType.Dagger)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorTest() throws InvalidArmorException {
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
    }

    @Test
    void equipAWrongArmorTest(){
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        try{
            r.equip(new Armor("Magic Cloth", 1, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Plate));  // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorNotRequiredLevelTest(){
        Rogue r = new Rogue("Valeera");                                                                 // Create a Rogue object
        try{
            r.equip(new Armor("Strong Leather", 2, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Leather)); // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void totalAttributesNoEquipmentTest(){
        Rogue r = new Rogue("Valeera");                              // Create a Rogue object
        assertEquals(2, r.totalAttributes().getStrength());      // Check strength
        assertEquals(6, r.totalAttributes().getDexterity());     // Check dexterity
        assertEquals(1, r.totalAttributes().getIntelligence());  // Check intelligence
    }

    @Test
    void totalAttributesEquippingArmorTest() throws InvalidArmorException {
        Rogue r = new Rogue("Valeera");                                                   // Create a Rogue object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        assertEquals(3, r.totalAttributes().getStrength());                             // Check strength
        assertEquals(7, r.totalAttributes().getDexterity());                            // Check dexterity
        assertEquals(2, r.totalAttributes().getIntelligence());                         // Check intelligence
    }

    @Test
    void totalAttributesEquippingTwoArmorsTest() throws InvalidArmorException {
        Rogue r = new Rogue("Valeera");                                                   // Create a Rogue object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        r.equip(new Armor("Strong Leather", 1, Slot.Head,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Leather)); // Equip armor
        assertEquals(5, r.totalAttributes().getStrength());                             // Check strength
        assertEquals(9, r.totalAttributes().getDexterity());                            // Check dexterity
        assertEquals(4, r.totalAttributes().getIntelligence());                         // Check intelligence
    }

    @Test
    void totalAttributesReplacingEquippingArmorTest() throws InvalidArmorException {
        Rogue r = new Rogue("Valeera");                                                   // Create a Rogue object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Mail));   // Equip armor
        assertEquals(4, r.totalAttributes().getStrength());                            // Check strength
        assertEquals(8, r.totalAttributes().getDexterity());                           // Check dexterity
        assertEquals(3, r.totalAttributes().getIntelligence());                        // Check intelligence
    }

    @Test
    void damageNoEquipmentTest(){
        Rogue r = new Rogue("Valeera");     // Create a Rogue object
        assertEquals(1.06f, r.damage());  // Check damage
    }

    @Test
    void damageEquippingWeaponTest() throws InvalidWeaponException {
        Rogue r = new Rogue("Valeera");                                                               // Create a Rogue object
        r.equip(new Weapon("Poison Dagger", 1, 2, Weapon.WeaponType.Dagger)); // Equip weapon
        assertEquals(2.12f, r.damage());                                                           // Check damage
    }

    @Test
    void damageReplacingEquippingWeaponTest() throws InvalidWeaponException {
        Rogue r = new Rogue("Valeera");                                                                // Create a Rogue object
        r.equip(new Weapon("Poison Dagger", 1, 2, Weapon.WeaponType.Dagger)); // Equip weapon
        r.equip(new Weapon("Powerful Sword", 1, 4, Weapon.WeaponType.Sword)); // Equip weapon
        assertEquals(4.24f, r.damage());                                                           // Check damage                                                             // Check damage
    }

    @Test
    void damageEquippingArmorAndWeaponTest() throws InvalidArmorException, InvalidWeaponException{
        Rogue r = new Rogue("Valeera");                                                               // Create a Rogue object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather));             // Equip armor
        r.equip(new Weapon("Powerful Sword", 1, 4, Weapon.WeaponType.Sword)); // Equip weapon
        assertEquals(4.28f, r.damage());
    }

    @Test
    void heroDisplayTest() {
        Rogue r = new Rogue("Valeera");                                                               // Create a Rogue object
        assertEquals("----------------------------------------\nName: Valeera\nClass: Rogue\nLevel: 1\nTotal strength: 2\nTotal dexterity: 6\nTotal intelligence: 1\nDamage: 1.06\n----------------------------------------", r.display());
    }
}