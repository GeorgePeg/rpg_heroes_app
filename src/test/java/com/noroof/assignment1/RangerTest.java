package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void constructorTest(){
        Ranger r = new Ranger("Hemet");                         // Create a Ranger object
        assertEquals("Hemet", r.name);                        // Check name
        assertEquals(1, r.level);                             // Check level
        assertEquals(1, r.levelAttributes.getStrength());     // Check strength
        assertEquals(7, r.levelAttributes.getDexterity());    // Check dexterity
        assertEquals(1, r.levelAttributes.getIntelligence()); // intelligence
    }

    @Test
    void levelUpTest() {
        Ranger r = new Ranger("Hemet");                           // Create a Ranger object
        r.levelUp();                                                    // Level up
        assertEquals(2, r.level);                               // Check level
        assertEquals(2, r.levelAttributes.getStrength());       // Check strength
        assertEquals(12, r.levelAttributes.getDexterity());     // Check dexterity
        assertEquals(2, r.levelAttributes.getIntelligence());   // intelligence
    }

    @Test
    void equipACorrectWeaponTest() throws InvalidWeaponException {
        Ranger r = new Ranger("Hemet");                                                         // Create a Ranger object
        r.equip(new Weapon("Silver Bow", 1, 2, Weapon.WeaponType.Bow)); // Equip weapon
    }

    @Test
    void equipAWrongWeaponTest(){
        Ranger r = new Ranger("Hemet");                                                                // Create a Mage object
        try {
            r.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectWeaponNotRequiredLevelTest(){
        Ranger r = new Ranger("Hemet");                                                                // Create a Ranger object
        try {
            r.equip(new Weapon("Golden Bow", 2, 2, Weapon.WeaponType.Bow)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorTest() throws InvalidArmorException {
        Ranger r = new Ranger("Hemet");                                                    // Create a Ranger object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
    }

    @Test
    void equipAWrongArmorTest(){
        Ranger r = new Ranger("Hemet");                                                         // Create a Ranger object
        try{
            r.equip(new Armor("Magic Cloth", 1, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Plate));  // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorNotRequiredLevelTest(){
        Ranger r = new Ranger("Hemet");                                                         // Create a Ranger object
        try{
            r.equip(new Armor("Strong Leather", 2, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Leather)); // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void totalAttributesNoEquipmentTest(){
        Ranger r = new Ranger("Hemet");                            // Create a Ranger object
        assertEquals(1, r.totalAttributes().getStrength());      // Check strength
        assertEquals(7, r.totalAttributes().getDexterity());     // Check dexterity
        assertEquals(1, r.totalAttributes().getIntelligence());  // Check intelligence
    }

    @Test
    void totalAttributesEquippingArmorTest() throws InvalidArmorException {
        Ranger r = new Ranger("Hemet");                                                   // Create a Ranger object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        assertEquals(2, r.totalAttributes().getStrength());                             // Check strength
        assertEquals(8, r.totalAttributes().getDexterity());                            // Check dexterity
        assertEquals(2, r.totalAttributes().getIntelligence());                         // Check intelligence
    }

    @Test
    void totalAttributesEquippingTwoArmorsTest() throws InvalidArmorException {
        Ranger r = new Ranger("Hemet");                                                   // Create a Ranger object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        r.equip(new Armor("Strong Leather", 1, Slot.Head,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Leather)); // Equip armor
        assertEquals(4, r.totalAttributes().getStrength());                             // Check strength
        assertEquals(10, r.totalAttributes().getDexterity());                            // Check dexterity
        assertEquals(4, r.totalAttributes().getIntelligence());                         // Check intelligence
    }

    @Test
    void totalAttributesReplacingEquippingArmorTest() throws InvalidArmorException {
        Ranger r = new Ranger("Hemet");                                                   // Create a Ranger object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather)); // Equip armor
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Mail));   // Equip armor
        assertEquals(3, r.totalAttributes().getStrength());                            // Check strength
        assertEquals(9, r.totalAttributes().getDexterity());                           // Check dexterity
        assertEquals(3, r.totalAttributes().getIntelligence());                        // Check intelligence
    }

    @Test
    void damageNoEquipmentTest(){
        Ranger r = new Ranger("Hemet");     // Create a Ranger object
        assertEquals(1.07f, r.damage());  // Check damage
    }

    @Test
    void damageEquippingWeaponTest() throws InvalidWeaponException {
        Ranger r = new Ranger("Hemet");                                                         // Create a Ranger object
        r.equip(new Weapon("Silver Bow", 1, 2, Weapon.WeaponType.Bow)); // Equip weapon
        assertEquals(2.14f, r.damage());                                                     // Check damage
    }

    @Test
    void damageReplacingEquippingWeaponTest() throws InvalidWeaponException {
        Ranger r = new Ranger("Hemet");                                                            // Create a Ranger object
        r.equip(new Weapon("Silver Bow", 1, 2, Weapon.WeaponType.Bow));    // Equip weapon
        r.equip(new Weapon("Powerful Bow", 1, 4, Weapon.WeaponType.Bow)); // Equip weapon
        assertEquals(4.28f, r.damage());                                                        // Check damage                                                             // Check damage
    }

    @Test
    void damageEquippingArmorAndWeaponTest() throws InvalidArmorException, InvalidWeaponException{
        Ranger r = new Ranger("Hemet");                                                            // Create a Ranger object
        r.equip(new Armor("Strong Leather", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Leather));         // Equip armor
        r.equip(new Weapon("Powerful Bow", 1, 4, Weapon.WeaponType.Bow)); // Equip weapon
        assertEquals(4.32f, r.damage());
    }

    @Test
    void heroDisplayTest() {
        Ranger r = new Ranger("Hemet");                                                 // Create a Mage object
        assertEquals("----------------------------------------\nName: Hemet\nClass: Ranger\nLevel: 1\nTotal strength: 1\nTotal dexterity: 7\nTotal intelligence: 1\nDamage: 1.07\n----------------------------------------", r.display());
    }
}