package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void constructorTest(){
        Mage m = new Mage("Jaina");                             // Create a Mage object
        assertEquals("Jaina", m.name);                        // Check name
        assertEquals(1, m.level);                             // Check level
        assertEquals(1, m.levelAttributes.getStrength());     // Check strength
        assertEquals(1, m.levelAttributes.getDexterity());    // Check dexterity
        assertEquals(8, m.levelAttributes.getIntelligence()); // intelligence
    }

    @Test
    void levelUpTest() {
        Mage m = new Mage("Jaina");                              // Create a Mage object
        m.levelUp();                                                   // Level up
        assertEquals(2, m.level);                              // Check level
        assertEquals(2, m.levelAttributes.getStrength());      // Check strength
        assertEquals(2, m.levelAttributes.getDexterity());     // Check dexterity
        assertEquals(13, m.levelAttributes.getIntelligence()); // intelligence
    }

    @Test
    void equipACorrectWeaponTest() throws InvalidWeaponException {
        Mage m = new Mage("Jaina");                                                                 // Create a Mage object
        m.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
    }

    @Test
    void equipAWrongWeaponTest(){
        Mage m = new Mage("Jaina");                                                                   // Create a Mage object
        try {
            m.equip(new Weapon("Sharp Axe", 1, 2, Weapon.WeaponType.Axe)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectWeaponNotRequiredLevelTest(){
        Mage m = new Mage("Jaina");                                                                   // Create a Mage object
        try {
            m.equip(new Weapon("Powerful Wand", 2, 2, Weapon.WeaponType.Axe)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorTest() throws InvalidArmorException {
        Mage m = new Mage("Jaina");                                                         // Create a Mage object
        m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Cloth)); // Equip armor
    }

    @Test
    void equipAWrongArmorTest(){
        Mage m = new Mage("Jaina");                                                           // Create a Mage object
        try {
            m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Plate));  // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorNotRequiredLevelTest(){
        Mage m = new Mage("Jaina");                                                           // Create a Mage object
        try {
            m.equip(new Armor("Magic Cloth", 2, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Plate)); // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void totalAttributesNoEquipmentTest(){
        Mage m = new Mage("Jaina");                                 // Create a Mage object
        assertEquals(1, m.totalAttributes().getStrength());      // Check strength
        assertEquals(1, m.totalAttributes().getDexterity());     // Check dexterity
        assertEquals(8, m.totalAttributes().getIntelligence());  // Check intelligence
    }

    @Test
    void totalAttributesEquippingArmorTest() throws InvalidArmorException {
        Mage m = new Mage("Jaina");                                                     // Create a Mage object
        m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Cloth)); // Equip armor
        assertEquals(2, m.totalAttributes().getStrength());                           // Check strength
        assertEquals(2, m.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(9, m.totalAttributes().getIntelligence());                       // Check intelligence
    }

    @Test
    void totalAttributesEquippingTwoArmorsTest() throws InvalidArmorException {
        Mage m = new Mage("Jaina ");                                                     // Create a Mage object
        m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Cloth)); // Equip armor
        m.equip(new Armor("Golden Cloth", 1, Slot.Head,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Cloth)); // Equip armor
        assertEquals(4, m.totalAttributes().getStrength());                           // Check strength
        assertEquals(4, m.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(11, m.totalAttributes().getIntelligence());                      // Check intelligence
    }

    @Test
    void totalAttributesReplacingEquippingArmorTest() throws InvalidArmorException {
        Mage m = new Mage("Jaina");                                                     // Create a Mage object
        m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Cloth)); // Equip armor
        m.equip(new Armor("Golden Cloth", 1, Slot.Body,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Cloth)); // Equip armor
        assertEquals(3, m.totalAttributes().getStrength());                           // Check strength
        assertEquals(3, m.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(10, m.totalAttributes().getIntelligence());                      // Check intelligence
    }

    @Test
    void damageNoEquipmentTest(){
        Mage m = new Mage("Jaina");         // Create a Mage object
        assertEquals(1.08f, m.damage());  // Check damage
    }

    @Test
    void damageEquippingWeaponTest() throws InvalidWeaponException {
        Mage m = new Mage("Jaina");                                                                // Create a Mage object
        m.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
        assertEquals(2.16f, m.damage());                                                          // Check damage
    }

    @Test
    void damageReplacingEquippingWeaponTest() throws InvalidWeaponException {
        Mage m = new Mage("Jaina");                                                                   // Create a Mage object
        m.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand));    // Equip weapon
        m.equip(new Weapon("Explosive Staff", 1, 4, Weapon.WeaponType.Staff)); // Equip weapon
        assertEquals(4.32f, m.damage());                                                               // Check damage
    }

    @Test
    void damageEquippingArmorAndWeaponTest() throws InvalidArmorException, InvalidWeaponException{
        Mage m = new Mage("Jaina");                                                                // Create a Mage object
        m.equip(new Armor("Magic Cloth", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Cloth));             // Equip armor
        m.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
        assertEquals(2.18f, m.damage());                                                          // Check damage
    }

    @Test
    void heroDisplayTest() {
        Mage m = new Mage("Jaina");                                                  // Create a Mage object
        assertEquals("----------------------------------------\nName: Jaina\nClass: Mage\nLevel: 1\nTotal strength: 1\nTotal dexterity: 1\nTotal intelligence: 8\nDamage: 1.08\n----------------------------------------", m.display());
    }
}