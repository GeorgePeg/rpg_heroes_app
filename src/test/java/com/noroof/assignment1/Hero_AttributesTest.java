package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Hero_AttributesTest {

    @Test
    void getStrength() {
        Hero m = new Mage("Jaina");                          // Create a Hero object
        assertEquals(1, m.levelAttributes.getStrength());  // Check strength
    }

    @Test
    void getDexterity() {
        Hero m = new Mage("Jaina");                          // Create a Hero object
        assertEquals(1, m.levelAttributes.getDexterity()); // Check dexterity
    }

    @Test
    void getIntelligence() {
        Hero m = new Mage("Jaina");                             // Create a Hero object
        assertEquals(8, m.levelAttributes.getIntelligence()); // intelligence
    }
}