package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void checkWeaponAttributesTest(){
        Weapon w = new Weapon("Powerful Wand", 3, 2, Weapon.WeaponType.Wand);   // Create weapon
        assertEquals("Powerful Wand", w.name);                                                        // Check name
        assertEquals(3, w.requiredLevel);                                                             // Check required level
        assertEquals(Slot.Weapon, w.slot);                                                                    // Check slot
        assertEquals(Weapon.WeaponType.Wand, w.weaponType);                                                   // Check weapon type
        assertEquals(2, w.weaponDamage);                                                              // Check weapon damage
    }

}