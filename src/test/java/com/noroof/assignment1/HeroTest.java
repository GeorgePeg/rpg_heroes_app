package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    @Test
    void constructorTest(){
        Hero m = new Mage("Jaina");                             // Create a Hero object
        assertEquals("Jaina", m.name);                        // Check name
        assertEquals(1, m.level);                             // Check level
        assertEquals(1, m.levelAttributes.getStrength());     // Check strength
        assertEquals(1, m.levelAttributes.getDexterity());    // Check dexterity
        assertEquals(8, m.levelAttributes.getIntelligence()); // intelligence
    }
}