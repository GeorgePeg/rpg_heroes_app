package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void checkItemAttributesTest(){
        Item w = new Weapon("Powerful Wand", 3, 2, Weapon.WeaponType.Wand);   // Create item
        assertEquals("Powerful Wand", w.name);                                                      // Check name
        assertEquals(3, w.requiredLevel);                                                           // Check required level
        assertEquals(Slot.Weapon, w.slot);                                                                  // Check slot
        assertEquals(Weapon.WeaponType.Wand, w.weaponType);                                                 // Check weapon type
        assertEquals(2, w.weaponDamage);                                                            // Check weapon damage
    }
}