package com.noroof.assignment1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void constructorTest(){
        Warrior w = new Warrior("Garrosh");                     // Create a Warrior object
        assertEquals("Garrosh", w.name);                      // Check name
        assertEquals(1, w.level);                             // Check level
        assertEquals(5, w.levelAttributes.getStrength());     // Check strength
        assertEquals(2, w.levelAttributes.getDexterity());    // Check dexterity
        assertEquals(1, w.levelAttributes.getIntelligence()); // intelligence
    }

    @Test
    void levelUpTest() {
        Warrior w = new Warrior("Garrosh");                       // Create a Warrior object
        w.levelUp();                                                    // Level up
        assertEquals(2, w.level);                               // Check level
        assertEquals(8, w.levelAttributes.getStrength());       // Check strength
        assertEquals(4, w.levelAttributes.getDexterity());      // Check dexterity
        assertEquals(2, w.levelAttributes.getIntelligence());   // intelligence
    }

    @Test
    void equipACorrectWeaponTest() throws InvalidWeaponException {
        Warrior w = new Warrior("Garrosh");                                                           // Create a Warrior object
        w.equip(new Weapon("Heavy Hammer", 1, 2, Weapon.WeaponType.Hammer)); // Equip weapon
    }

    @Test
    void equipAWrongWeaponTest(){
        Warrior w = new Warrior("Garrosh");                                                           // Create a Warrior object
        try {
            w.equip(new Weapon("Powerful Wand", 1, 2, Weapon.WeaponType.Wand)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectWeaponNotRequiredLevelTest(){
        Warrior w = new Warrior("Garrosh");                                                           // Create a Warrior object
        try {
            w.equip(new Weapon("Heavy Hammer", 2, 2, Weapon.WeaponType.Hammer)); // Equip weapon
        }catch (Exception e){
            assertEquals("Exception: This weapon can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorTest() throws InvalidArmorException {
        Warrior w = new Warrior("Garrosh");                                             // Create a Warrior object
        w.equip(new Armor("Warm Plate", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Plate)); // Equip armor
    }

    @Test
    void equipAWrongArmorTest(){
        Warrior w = new Warrior("Garrosh");                                             // Create a Warrior object
        try{
            w.equip(new Armor("Magic Cloth", 1, Slot.Body,
                    new Hero_Attributes(1, 1, 1), Armor.ArmorType.Cloth));  // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void equipACorrectArmorNotRequiredLevelTest(){
        Warrior w = new Warrior("Garrosh");                                                  // Create a Warrior object
        try{
            w.equip(new Armor("Warm Plate", 2, Slot.Body,
                    new Hero_Attributes(1,1,1), Armor.ArmorType.Plate)); // Equip armor
        }catch (Exception e){
            assertEquals("Exception: This armor can't be used by this hero or you are below the required level", e.getMessage());
        }
    }

    @Test
    void totalAttributesNoEquipmentTest(){
        Warrior w = new Warrior("Garrosh");                        // Create a Warrior object
        assertEquals(5, w.totalAttributes().getStrength());      // Check strength
        assertEquals(2, w.totalAttributes().getDexterity());     // Check dexterity
        assertEquals(1, w.totalAttributes().getIntelligence());  // Check intelligence
    }

    @Test
    void totalAttributesEquippingArmorTest() throws InvalidArmorException {
        Warrior w = new Warrior("Garrosh");                        // Create a Warrior object
        w.equip(new Armor("Warm Plate", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Plate)); // Equip armor
        assertEquals(6, w.totalAttributes().getStrength());                           // Check strength
        assertEquals(3, w.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(2, w.totalAttributes().getIntelligence());                       // Check intelligence
    }

    @Test
    void totalAttributesEquippingTwoArmorsTest() throws InvalidArmorException {
        Warrior w = new Warrior("Garrosh");                                              // Create a Warrior object
        w.equip(new Armor("Elastic Mail", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Mail)); // Equip armor
        w.equip(new Armor("Warm Plate", 1, Slot.Legs,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Plate)); // Equip armor
        assertEquals(8, w.totalAttributes().getStrength());                           // Check strength
        assertEquals(5, w.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(4, w.totalAttributes().getIntelligence());                       // Check intelligence
    }

    @Test
    void totalAttributesReplacingEquippingArmorTest() throws InvalidArmorException {
        Warrior w = new Warrior("Garrosh");                                              // Create a Warrior object
        w.equip(new Armor("Elastic Mail", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Mail)); // Equip armor
        w.equip(new Armor("Warm Plate", 1, Slot.Body,
                new Hero_Attributes(2,2,2), Armor.ArmorType.Plate)); // Equip armor
        assertEquals(7, w.totalAttributes().getStrength());                           // Check strength
        assertEquals(4, w.totalAttributes().getDexterity());                          // Check dexterity
        assertEquals(3, w.totalAttributes().getIntelligence());                       // Check intelligence
    }

    @Test
    void damageNoEquipmentTest(){
        Warrior w = new Warrior("Garrosh");  // Create a Warrior object
        assertEquals(1.05f, w.damage());  // Check damage
    }

    @Test
    void damageEquippingWeaponTest() throws InvalidWeaponException {
        Warrior w = new Warrior("Garrosh");                                                          // Create a Warrior object
        w.equip(new Weapon("Heavy Hammer", 1, 2, Weapon.WeaponType.Hammer)); // Equip weapon
        assertEquals(2.1f, w.damage());                                                            // Check damage
    }

    @Test
    void damageReplacingEquippingWeaponTest() throws InvalidWeaponException {
        Warrior w = new Warrior("Garrosh");                                                              // Create a Warrior object
        w.equip(new Weapon("Heavy Hammer", 1, 2, Weapon.WeaponType.Hammer));     // Equip weapon
        w.equip(new Weapon("Powerful Hammer", 1, 4, Weapon.WeaponType.Hammer)); // Equip weapon
        assertEquals(4.2f, w.damage());                                                               // Check damage                                                             // Check damage
    }

    @Test
    void damageEquippingArmorAndWeaponTest() throws InvalidArmorException, InvalidWeaponException{
        Warrior w = new Warrior("Garrosh");                                                             // Create a Warrior object
        w.equip(new Armor("Warm Plate", 1, Slot.Body,
                new Hero_Attributes(1,1,1), Armor.ArmorType.Plate));                 // Equip armor
        w.equip(new Weapon("Powerful Hammer", 1, 4, Weapon.WeaponType.Hammer)); // Equip weapon
        assertEquals(4.24f, w.damage());
    }

    @Test
    void heroDisplayTest() {
        Warrior w = new Warrior("Garrosh");                                                             // Create a Warrior object
        assertEquals("----------------------------------------\nName: Garrosh\nClass: Warrior\nLevel: 1\nTotal strength: 5\nTotal dexterity: 2\nTotal intelligence: 1\nDamage: 1.05\n----------------------------------------", w.display());
    }
}