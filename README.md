# **RPG_Heroes_App**<br>

## Below there are images of tables that describe entities (classes) that their used on this project
<img src="/pictures/HeroTable.PNG">
<img src="/pictures/Mage_all.PNG">
<img src="/pictures/Ranger_all.PNG">
<img src="/pictures/Rogue_all.PNG">
<img src="/pictures/Warrior_all.PNG">
<img src="/pictures/Hero_AttributesTable.PNG">
<img src="/pictures/SlotTable.PNG">
<img src="/pictures/ItemTable.PNG"><br>
<img src="/pictures/ArmorTable.PNG">
<img src="/pictures/InvalidArmorExceptionTable.PNG"><br>
<img src="/pictures/WeaponTable.PNG">
<img src="/pictures/InvalidWeaponExceptionTable.PNG">